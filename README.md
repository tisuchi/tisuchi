#  Test of iMoney Senior Software Enginner

## Instructions

We expect you to be familiar with git and Docker. 

The question and initial code is provided to you on this git repository. 

1. Fork the repository
2. Develop your code
3. Send a pull request
 
You can commit and push as many times as you want.

## Intro and background info

You need to build a lead (information containing name, email, phone etc) collection system. Operator can add leads and see the list of the leads.

## Goal 1
Prepare a form using AngularJS 1.x. The form should contain

- Name
- Email Address
- Phone number
- Date of birth

Submitting the form will consume an API (Goal 3) to save the information as well as update the list of leads (Goal 2).

## Goal 2

Prepare the list of saved leads with pagination with Angular JS 1.x. This list will get the data from an API (Goal 3).

## Goal 3
Prepare 2 APIs using Silex framework of php
* to save a lead to MongoDB
* to retrieve the leads from MongoDB

## Bonus
Write unit tests for your functions.

## Evaluation Criteria 
- How quick can you learn and implement. 
- Quality of your code 
- Understanding the features.

## Requirements
### Mac
[Docker for Mac](https://download.docker.com/mac/stable/Docker.dmg)
### Windows
[Docker for Windows](https://download.docker.com/win/stable/InstallDocker.msi)
### Linux
Follow the instructions laid out [here](https://docs.docker.com/engine/installation/linux/) officially by Docker.

## Installation (Mac and Linux)

1. Clone this repository.

2. Make utilities script executable.

        $ chmod +x docker-utilities.sh

3. Run all the containers.

		$ ./docker-utilities.sh run

4. Tunnel into php container and do a composer install.

		$ composer install

## Running the server

1. Run the server by using the utilities shell script included

        $ ./docker-utilities.sh run

2. Add in these lines into your /etc/hosts file

        127.0.0.1  dev.project.test

3. Access main page by navigating you browser to [http://dev.project.test](http://dev.project.test)

## Tunneling into container

This docker compose is made up of multiple containers namely [NGINX](https://hub.docker.com/_/nginx/), [PHP](https://hub.docker.com/_/php/) and [MongoDB](https://hub.docker.com/_/mongo/). To tunnel into any of these containers we need to use these commands.

For [NGINX](https://hub.docker.com/_/nginx/),

	$ docker-compose exec nginx sh

For [PHP](https://hub.docker.com/_/php/),

	$ docker-compose exec php bash

For [MongoDB](https://hub.docker.com/_/mongo/),

	$ docker-compose exec db bash

## FAQ

1. Where do I start to code?

		$ project/web

1. How do I stop all the containers?

		$ docker-compose stop

1. How do i reset all containers to its initial factory state?

	Firstly stop all the containers that is running in docker. Then run this `kill and cleanup` command,

		$ ./docker-utilities.sh kill && ./docker-utilities.sh cleanup

	Check if images and volumes are all cleaned by running this command,

		$ docker images

	and

		$ docker volume ls

	If there are still any images and/or volumes that is not cleared, run the `kill and cleanup` command again.

1. Why can't I connect to MongoDB from my Silex application?

	In docker, to connect to MongoDB using a connection string, the string needs to be in this form.

		mongodb://mongo:27017
